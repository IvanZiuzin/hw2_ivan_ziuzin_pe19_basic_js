"use strict"
// Этот код работает в современном режиме


// ПРАКТИЧЕСКОЕ ЗАДАНИЕ  ------------------------

// Технические требования к заданию:
// Получить при помощи модального окна браузера данные пользователя: Имя и Возраст.
// Если возраст меньше 18 лет, - показать на экране уведомление: You are not allowed to visit this website! :(
// Если возраст от 18 до 22 лет(включительно), - показать окно с следующим уведомлением: Are you sure you want to continue? и кнопками Ok и Cancel. 
// Если пользователь нажал Ok, - показать на экране уведомление welcome + Имя пользователя.Если пользователь нажал Cancel, 
// - показать на экране уведомление You are not allowed to visit this website!:(.
// Если возраст пользователя больше 22 лет - показать на экране уведомление Welcome, + Имя пользователя
// Обязательно необходимо использорвать синтаксис ES6 (ES2015) для создания переменных
// после введения данных, добавить проверку их корректности. Если пользователь не ввел Имя, или при введении возраста, указал не число, 
// - спросить пользователя: Имя и возраст снова (при этом дефолтным значением для каждой из переменных, должна быть ранее введенная информация)

// Мои примечания: 
// 1. Нужна проверка на ввод формата строки для имени и фамилии
// 2. Нужна проверка на ввод формата числа для возраста  
// 3. Сделать вопросы обязательными - без корректного заполнения 


// 1.Переменные: 

// 1.1. Имя пользователя
// Переменные имени:
let userFirstName
let userNameCheck
// Проверка корректности ввода имени пользователя:
do{
    userFirstName = prompt('Please, enter your first name to continue', '');
    userNameCheck = Number(userFirstName);
}
while (!isNaN(userNameCheck));

// 1.2. Возраст пользователя

// Переменные возраста:
let userAge
let userAgeCheck
// Проверка корректности ввода возраста пользователя:
do{
    userAge = prompt(`${userFirstName}, enter your current age to continue, please`,'Use only figures');
    userAgeCheck = Number(userAge);
}
while(isNaN(userAgeCheck));


//1.3 Константы - стандартизированные фразы, в зависимости от сценария: 

const accessAllowed = 'Welcome to our site! :)';
const accessConfirm = 'Are you sure you want to continue?';
const accessForbidden  = 'You are not allowed to visit this website! :(';

// 1.4. Приветствие пользователя в зависимости от возраста

let greetingMassege = (userAge < 18) ? 'Sorry, kid! :)':
    (userAge <= 22) ? 'Hi buddy! :)': 
    'Hello,';

let massage
let confirmMassage

// 1.5. Условная функция "If"

if(userAge < 18){
    massage = `${greetingMassege}\n${accessForbidden}`;
} else if(userAge <=22){
    confirmMassage = confirm(`${greetingMassege}\n${accessConfirm}`);
    if(confirmMassage === true){
        massage = `${userFirstName}\n${accessAllowed}`;
    } else {
        massage = `${userFirstName}\n${accessForbidden}`;
    }
} else {
    massage = `${greetingMassege}\n${userFirstName}\n${accessAllowed}`;
}

// АЛЕРТЫ:

alert(massage);

// КОНСОЛЬ:

console.log(typeof userFirstName, userFirstName);
console.log(typeof userNameCheck, userNameCheck);
console.log(typeof userAge, userAge);
console.log(typeof userAgeCheck, userAgeCheck);
console.log(typeof greetingMassege, greetingMassege);
console.log(typeof confirmMassage, confirmMassage);
console.log(typeof massage, massage);

console.log("The end ot HW2:-)");









